# Generated from after_commit-1.0.10.gem by gem2rpm -*- rpm-spec -*-
%global gem_name after_commit
%if 0%{?el6}
%global rubyabi 1.8
%else
%global rubyabi 1.9.1
%endif

Summary: after_commit callback for ActiveRecord
Name: rubygem-%{gem_name}
Version: 1.0.10
Release: 1%{?dist}
Group: Development/Languages
License: GPLv2+ or Ruby
URL: http://github.com/freelancing-god/after_commit
Source0: %{gem_name}-%{version}.gem
Requires: ruby(abi) = %{rubyabi}
Requires: ruby(rubygems) 
Requires: rubygem(activerecord) < 3.0.0
Requires: rubygem(activerecord) >= 1.15.6
BuildRequires: ruby(abi) = %{rubyabi}
%if 0%{?fedora}
BuildRequires: rubygems-devel
%else
BuildRequires: ruby(rubygems)
%endif
# Tests:
BuildRequires: rubygem(minitest)
BuildRequires: rubygem(activerecord)
%if 0%{?el6}
BuildRequires: rubygem(sqlite3-ruby)
%else
BuildRequires: rubygem(sqlite3)
%endif
BuildArch: noarch
Provides: rubygem(%{gem_name}) = %{version}

# macros for RHEL6 compatibility:
%{!?gem_dir: %global gem_dir %(ruby -rubygems -e 'puts Gem::dir' 2>/dev/null)}
%{!?gem_instdir: %global gem_instdir %{gem_dir}/gems/%{gem_name}-%{version}}
%{!?gem_libdir: %global gem_libdir %{gem_instdir}/lib}
%{!?gem_cache: %global gem_cache %{gem_dir}/cache/%{gem_name}-%{version}.gem}
%{!?gem_spec: %global gem_spec %{gem_dir}/specifications/%{gem_name}-%{version}.gemspec}
%{!?gem_docdir: %global gem_docdir %{gem_dir}/doc/%{gem_name}-%{version}}
%{!?gem_extdir: %global gem_extdir %{_libdir}/gems/exts/%{gem_name}-%{version}}

%description
A Ruby on Rails plugin to add an after_commit callback. This can be used
to trigger methods only after the entire transaction is complete.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}

%prep
%setup -q -c -T
mkdir -p .%{gem_dir}
gem install --local --install-dir .%{gem_dir} \
            --force %{SOURCE0}

%build

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/

%check
pushd .%{gem_instdir}
  testrb test/*_test.rb
popd


%files
%dir %{gem_instdir}
%doc %{gem_instdir}/LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/README.textile
%doc %{gem_instdir}/test

%changelog
* Fri Feb 15 2013 ktdreyer@ktdreyer.com - 1.0.10-1
- Initial package, created with gem2rpm 0.8.1
